# Shape Correspondences using Spiral Sequences

This repository contains the code to reproduce the results for the
sequence-based networks in the paper

Lim, I., Dielen, A., Campen, M., & Kobbelt, L. (2018). A Simple Approach to
Intrinsic Correspondence Learning on Unstructured 3D Meshes. arXiv preprint
arXiv:1809.06664.


## Results

![](data/results.png)


## Dependencies

* [pytorch][pytorch] 0.4.1
* [openmesh][openmesh]
* [h5py][h5py]
* [matplotlib][matplotlib]


## Dataset

To run the code you need a copy of the meshes in the [FAUST][faust] dataset and
the precomputed SHOT descriptors. Both are contained in this [archive][dropbox]
originally posted by Jonathan Masci [here][tutorial]. Simply extract the
`EG16_tutorial` folder to `data/` and you should be set.

Alternatively you can run the preprocessing code of Masci et al. yourself. The
code for computing the SHOT descriptor can be found [here][matlab1]. We computed
geodesic distances using the code from [here][matlab2]. Copy the mesh files to
`data/meshes/`, the shot descriptors to `data/shot/` and the geodesic distances
to `data/dists/`.

Precomputed distances are optional and only required for evaluation purposes
(i.e. the graph above).


## Running the code

    python train.py --mode lstm

    python train.py --mode linear --epochs 4000

Model checkpoints and predictions on the validation and test sets are saved
to `out/`.

Note: On the first run, the spiral sequences for all meshes and all possible
rotations are precomputed. This may take a while. You can control the number of
parallel processes using the `--processes N` option (default: 4).


[pytorch]: https://pytorch.org/
[openmesh]: https://pypi.org/project/openmesh/
[h5py]: https://pypi.org/project/h5py/
[matplotlib]: https://pypi.org/project/matplotlib/
[faust]: http://faust.is.tue.mpg.de/
[dropbox]: https://www.dropbox.com/s/aamd98nynkvbcop/EG16_tutorial.tar.bz2?dl=0
[tutorial]: https://github.com/jonathanmasci/EG16_tutorial/blob/master/deep_learning_for_3D_shape_analysis.ipynb
[matlab1]: https://github.com/davideboscaini/shape_utils
[matlab2]: https://github.com/jonathanmasci/ShapeNet_data_preparation_toolbox
