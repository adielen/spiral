import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim

import numpy as np

import sys
import os
import time
import random
import argparse

import faust_data
import utils


class SpiralLayer(torch.nn.Module):
    def __init__(self, linear, in_features, out_features, seq_length):
        super(SpiralLayer, self).__init__()
        if linear:
            self.layer = nn.Linear(in_features * seq_length, out_features)
            torch.nn.init.xavier_uniform_(self.layer.weight, gain=1)
            torch.nn.init.constant_(self.layer.bias, 0)
        else:
            self.layer = nn.LSTM(in_features, out_features, batch_first=True)
        self.linear = linear

    def forward(self, x, indices):
        bs, seq_length = indices.size()
        x = torch.index_select(x, 0, indices.view(-1))
        if self.linear:
            x = x.view(bs, -1)
            x = self.layer(x)
        else:
            x = x.view(bs, seq_length, -1)
            x = self.layer(x)[1][0]
            x = x.squeeze()
        return x


class SpiralNet(torch.nn.Module):
    def __init__(self, linear, in_features=544, n_classes=6890, seq_length=30):
        super(SpiralNet, self).__init__()
        outputs = [100, 150, 200] if linear else [150, 200, 250]
        self.fc1 = nn.Linear(in_features, 16)
        self.spiral1 = SpiralLayer(linear, 16, outputs[0], seq_length)
        self.spiral2 = SpiralLayer(linear, outputs[0], outputs[1], seq_length)
        self.spiral3 = SpiralLayer(linear, outputs[1], outputs[2], seq_length)
        self.fc2 = nn.Linear(outputs[2], 256)
        self.fc3 = nn.Linear(256, n_classes)

        if linear:
            torch.nn.init.xavier_uniform_(self.fc1.weight, gain=1)
            torch.nn.init.xavier_uniform_(self.fc2.weight, gain=1)
            torch.nn.init.xavier_uniform_(self.fc3.weight, gain=1)
            torch.nn.init.constant_(self.fc1.bias, 0)
            torch.nn.init.constant_(self.fc2.bias, 0)
            torch.nn.init.constant_(self.fc3.bias, 0)

    def forward(self, x, indices):
        x = F.dropout(x, p=0.3, training=self.training)
        x = F.relu(self.fc1(x))
        x = F.relu(self.spiral1(x, indices))
        x = F.relu(self.spiral2(x, indices))
        x = F.relu(self.spiral3(x, indices))
        x = F.dropout(x, p=0.3, training=self.training)
        x = self.fc2(x)
        x = F.dropout(x, p=0.3, training=self.training)
        x = self.fc3(x)
        return x


def train(net, data, targets, device, criterion, optimizer, epoch, n_epochs):
    net.train()
    losses, accuracies = [], []
    for i in np.random.permutation(len(data)):
        x, indices, _ = data[i]
        x = torch.from_numpy(x).to(device)
        indices = torch.from_numpy(indices).to(device)
        optimizer.zero_grad()
        outputs = net(x, indices)
        predictions = outputs.argmax(1)
        correct = predictions.eq(targets).sum().item()
        loss = criterion(outputs, targets)
        loss.backward()
        optimizer.step()
        losses.append(loss.item())
        accuracies.append(correct / len(predictions))
    print('========================================')
    print('Epoch {} of {}'.format(epoch, n_epochs))
    print('========================================')
    print('Training Set')
    print('  Loss:     {0:.15f}'.format(np.mean(losses)))
    print('  Accuracy: {0:.15f}'.format(np.mean(accuracies)))


def validate(net, data, targets, device, criterion):
    net.eval()
    losses, accuracies = [], []
    with torch.no_grad():
        for i in range(len(data)):
            x, indices, _ = data[i]
            x = torch.from_numpy(x).to(device)
            indices = torch.from_numpy(indices).to(device)
            outputs = net(x, indices)
            predictions = outputs.argmax(1)
            correct = predictions.eq(targets).sum().item()
            loss = criterion(outputs, targets)
            losses.append(loss.item())
            accuracies.append(correct / len(predictions))
    print('Validation Set')
    print('  Loss:     {0:.15f}'.format(np.mean(losses)))
    print('  Accuracy: {0:.15f}\n'.format(np.mean(accuracies)))
    sys.stdout.flush()
    return np.mean(accuracies)


def save_predictions(net, data, device, predictions_dir):
    net.eval()
    with torch.no_grad():
        for i in range(len(data)):
            x, indices, filename = data[i]
            x = torch.from_numpy(x).to(device)
            indices = torch.from_numpy(indices).to(device)
            outputs = net(x, indices)
            predictions = outputs.argmax(1)
            filename = predictions_dir + filename + '.npy'
            np.save(filename, predictions.cpu().numpy())


def save_checkpoint(net, optimizer, epoch, checkpoint_dir):
    torch.save({
        'model_state_dict': net.state_dict(),
        'optimizer_state_dict': optimizer.state_dict(),
        'epoch': epoch,
    }, checkpoint_dir + 'checkpoint.tar')


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--mode', type=str, default='lstm')
    parser.add_argument('--epochs', type=int, default=1000)
    parser.add_argument('--lr', type=float, default=0.001)
    parser.add_argument('--seq_length', type=int, default=30)
    parser.add_argument('--processes', type=int, default=4)
    parser.add_argument('--checkpoint', type=str)
    return parser.parse_args()


def save_args(filename, args):
    args_list = ['{}: {}'.format(arg, getattr(args, arg)) for arg in vars(args)]
    args_text = '\n'.join(sorted(args_list))
    text_file = open(filename, 'w')
    text_file.write(args_text)
    text_file.close()


def main():
    # make reproducible
    random.seed(42)
    np.random.seed(42)
    torch.manual_seed(42)
    torch.cuda.manual_seed_all(42)

    run_dir = 'out/' + time.strftime('%Y-%m-%d-%H-%M-%S/', time.localtime())
    checkpoint_dir = run_dir + 'checkpoint/'
    predictions_dir = run_dir + 'predictions/'

    if not os.path.exists(checkpoint_dir):
        os.makedirs(checkpoint_dir)

    if not os.path.exists(predictions_dir):
        os.makedirs(predictions_dir)

    args = parse_args()
    save_args(run_dir + 'args.txt', args)

    device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

    net = SpiralNet(args.mode=='linear').to(device)
    criterion = nn.CrossEntropyLoss()
    optimizer = optim.Adam(net.parameters(), lr=args.lr)

    n_params = sum(p.numel() for p in net.parameters() if p.requires_grad)
    print('Number of trainable parameters: {}'.format(n_params))

    # restore parameters & epoch from checkpoint
    if args.checkpoint is not None:
        checkpoint = torch.load(args.checkpoint)
        net.load_state_dict(checkpoint['model_state_dict'])
        optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
        start_epoch = checkpoint['epoch'] + 1
    else:
        start_epoch = 1

    print('Loading data...\n')
    faust_data.precompute_spirals(args.seq_length, args.processes)
    training_dataset = faust_data.FaustDataset('training', args.seq_length)
    validation_dataset = faust_data.FaustDataset('validation', args.seq_length)
    test_dataset = faust_data.FaustDataset('test', args.seq_length)

    targets = torch.arange(6890, dtype=torch.long).to(device)
    max_acc = 0

    for epoch in range(start_epoch, args.epochs + 1):
        train(net, training_dataset, targets, device, criterion, optimizer, epoch, args.epochs)
        acc = validate(net, validation_dataset, targets, device, criterion)
        if epoch > args.epochs // 4 and acc > max_acc:
            max_acc = acc
            save_predictions(net, test_dataset, device, predictions_dir)
            save_checkpoint(net, optimizer, epoch, checkpoint_dir)

    if faust_data.geodesic_dists_available():
        errors = []
        targets = targets.cpu().numpy()
        for i in range(*faust_data.split['test']):
            filename = predictions_dir + 'tr_reg_{:03}.npy'.format(i)
            predictions = np.load(filename)
            dists = faust_data.geodesic_dists(i)
            errors.append(dists[targets, predictions])
        errors = np.concatenate(errors)
        utils.save_benchmark(run_dir + 'results.pdf', errors)


if __name__ == "__main__":
    main()
