import os
import random
import functools
import multiprocessing

import pickle
import h5py

import numpy as np

from torch.utils.data import Dataset
from utils import extract_and_save


split = {
    'training': (0, 70),
    'validation': (70, 80),
    'test': (80, 100)
}

data_dir = 'data/'
spiral_dir = data_dir + 'spirals/'

if os.path.exists(data_dir + 'meshes/'):
    mesh_dir = data_dir + 'meshes/'
else:
    mesh_dir = data_dir + 'EG16_tutorial/dataset/FAUST_registrations/' \
                          'meshes/orig/'

if os.path.exists(data_dir + 'shot/'):
    desc_dir = data_dir + 'shot/'
else:
    desc_dir = data_dir + 'EG16_tutorial/dataset/FAUST_registrations/' \
                          'data/diam=200/descs/shot/'


class FaustDataset(Dataset):
    def __init__(self, mode, seq_length):
        self.descriptors = []
        self.indices = []
        self.filenames = []

        for i in range(*split[mode]):
            # load descriptor
            filename = desc_dir + 'tr_reg_{:03}.mat'.format(i)
            f = h5py.File(filename, 'r')
            desc = np.array(f['desc'], dtype=np.float32).T
            desc = np.ascontiguousarray(desc)
            self.descriptors.append(desc)
            f.close()
            # load spiral
            filename = spiral_dir + '{}/tr_reg_{:03}.pkl'.format(seq_length, i)
            self.indices.append(pickle.load(open(filename, 'rb')))
            # remember filename
            self.filenames.append('tr_reg_{:03}'.format(i))


    def __len__(self):
        return len(self.descriptors)


    def __getitem__(self, idx):
        indices = []
        for i in range(len(self.indices[idx])):
            pick = random.randint(0, len(self.indices[idx][i]) - 1)
            indices.append(self.indices[idx][i][pick])
        indices = np.array(indices, dtype=np.int64)
        return self.descriptors[idx], indices, self.filenames[idx]


def precompute_spirals(seq_length, processes):
    output_dir = spiral_dir + '{}/'.format(seq_length)
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    func = functools.partial(extract_and_save, seq_length=seq_length,
        input_dir=mesh_dir, output_dir=output_dir)
    pool = multiprocessing.Pool(processes)
    pool.map(func, list(range(100)))
    pool.close()
    pool.join()


def geodesic_dists_available():
    return os.path.exists(data_dir + 'dists/')


def geodesic_dists(idx):
    filename = data_dir + 'dists/tr_reg_{:03}.mat'.format(idx)
    f = h5py.File(filename, 'r')
    dist = np.array(f['dist'])
    f.close()
    return dist
